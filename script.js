const seviceMenu = Array.from(
  document.getElementsByClassName("service-menu-item")
);
const seviceInfo = Array.from(document.getElementsByClassName("service-info"));
const seviceContainer = document.querySelector(".service");
const ourWorkContainer = document.querySelector(".our-work-menu");
const ourWorkMenu = Array.from(
  document.getElementsByClassName("work-menu-item")
);
const loadBtn = document.querySelector(".load-btn");
const hiddenImgs = document.querySelector(".hidden-imgs");
const imgBlock = document.querySelector(".our-work-images");

seviceContainer.addEventListener("click", function(event) {
  event.preventDefault();
  if (event.target.classList.contains("service-menu-item")) {
    changeActive(event, seviceMenu);
    seviceInfo.forEach(item => {
      item.classList.remove("active");
    });
    seviceInfo[seviceMenu.indexOf(event.target)].classList.add("active");
  }
});

ourWorkContainer.addEventListener("click", function(event) {
  event.preventDefault();
  if (event.target.classList.contains("work-menu-item")) {
    changeActive(event, ourWorkMenu);
    const imgItem = Array.from(
      document.getElementsByClassName("work-img-container")
    );
    const dataSection = event.target.dataset.section;
    imgItem.forEach(item => {
      item.classList.remove("active");
    });

    imgItem.forEach(item => {
      if (item.dataset.section === dataSection) {
        item.classList.add("active");
      }
      if (!dataSection) {
        item.classList.add("active");
      }

      if (event.target.classList.contains("active") && dataSection) {
        loadBtn.classList.remove("active");
      } else {
        loadBtn.classList.add("active");
      }
    });
  }
});

loadBtn.addEventListener("click", function(event) {
  event.preventDefault();
  const images = Array.from(hiddenImgs.children);
  images.forEach(img => {
    img.classList.add("active");
    if (img.classList.contains("active")) {
      console.log(img);
      imgBlock.appendChild(img);
    }
  });
  this.remove();
});

function changeActive(event, menuItem) {
  menuItem.forEach(item => {
    item.classList.remove("active");
  });
  event.target.classList.add("active");
}

/* S-l-i-d-e-r */

const mySwiper = new Swiper(".swiper-container", {
  direction: "horizontal",
  loop: true,

  pagination: {
    el: ".swiper-pagination",
    clickable: true,
    renderBullet: function(index, className) {
      return '<span class="' + className + '">' + "</span>";
    }
  },

  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev"
  },

  scrollbar: {
    el: ".swiper-scrollbar"
  },
  autoplay: {
    delay: 5000
  }
});
